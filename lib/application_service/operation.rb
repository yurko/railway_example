require "dry/transaction/operation"

module ApplicationService
  class Operation
    include Dry::Transaction::Operation

    class << self
      delegate :call, to: :new
    end

    def call(input)
      raise NotImplementedError.new("Operation must implement #call")
    end
  end
end
