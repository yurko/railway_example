module ApplicationService
  class Base
    class << self
      extend Forwardable
      def_delegator :new, :call
    end
  end
end
