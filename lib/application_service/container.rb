require "dry/container"
require "dry/monads"

module ApplicationService
  class Container
    extend Dry::Container::Mixin
    extend Dry::Monads::Result::Mixin

    register(:transaction) do |input, &block|
      result = nil

      ActiveRecord::Base.transaction do
        result = block.call(Success(input)) # goes through all steps
        raise ActiveRecord::Rollback if result.failure?
      end

      result
    end
  end
end
