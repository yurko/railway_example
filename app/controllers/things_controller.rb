class ThingsController < ApplicationController
  def create
    Things::Create.call(params) do |result|
      result.success do |value|
        render json: value, status: :created
      end

      result.failure do |error|
        render json: error.value, status: :unprocessable_entity
      end
    end
  end
end
