require "dry/transaction"

module Things
  class Purchase < ApplicationService::Base
    include Dry::Transaction(container: Container)

    around :transaction
    step :check_availability
    step :create_order
    step :charge_user
    step :add_vendor_sale
    step :create_shipment
    step :notify_user
    step :notify_vendor
    step :respond_with_success_message
  end
end
