require "dry/transaction"

module Things
  class Create < ApplicationService::Base
    include Dry::Transaction(container: Things::Create::Container)

    around :transaction
    step :process
    step :validate
    step :persist
  end
end
