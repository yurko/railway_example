module Things
  class Purchase
    class Container < ApplicationService::Container
      register :check_availability, Operations::CheckAvailability
      register :create_order, Operations::CreateOrder
      register :charge_user, Operations::ChargeUser
      register :add_vendor_sale, Operations::AddVendorSale
      register :create_shipment, Operations::CreateShipment
      register :notify_user, Operations::NotifyUser
      register :notify_vendor, Operations::NotifyVendor
      register :respond_with_success_message, Operations::RespondWithSuccessMessage
    end
  end
end
