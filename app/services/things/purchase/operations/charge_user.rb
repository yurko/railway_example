module Things
  class Purchase
    module Operations
      class ChargeUser < ApplicationService::Operation
        def call(input)
          Success(input)
        end
      end
    end
  end
end
