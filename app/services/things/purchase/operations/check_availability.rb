module Things
  class Purchase
    module Operations
      class CheckAvailability < ApplicationService::Operation
        def call(input)
          Success(input)
        end
      end
    end
  end
end
