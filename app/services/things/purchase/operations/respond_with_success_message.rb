module Things
  class Purchase
    module Operations
      class RespondWithSuccessMessage < ApplicationService::Operation
        def call(input)
          Success({message: "Success!"})
        end
      end
    end
  end
end
