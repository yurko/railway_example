module Things
  class Purchase
    module Operations
      class CreateOrder < ApplicationService::Operation
        def call(input)
          Success(input)
        end
      end
    end
  end
end
