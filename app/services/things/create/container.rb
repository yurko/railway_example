module Things
  class Create
    class Container < ApplicationService::Container
      register :process, Create::Operations::Process
      register :validate, Create::Operations::Validate
      register :persist, Create::Operations::Persist
    end
  end
end
