module Things
  class Create
    module Operations
      class Validate < ApplicationService::Operation
        def call(input)
          if input[:email].present?
            Success(input)
          else
            Failure(:not_valid)
          end
        end
      end
    end
  end
end
