module Things
  class Create
    module Operations
      class Process < ApplicationService::Operation
        def call(input)
          Success(name: input["name"], email: input["email"])
        end
      end
    end
  end
end
