module Things
  class Create
    module Operations
      class Persist < ApplicationService::Operation
        def call(input)
          [] << input
          input[:message] = :created

          Success(input)
        end
      end
    end
  end
end
